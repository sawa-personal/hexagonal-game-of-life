//*******************************************************
// State.java
// created by Sawada Tatsuki on 2018/06/05.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* 状態の列挙型 */

public enum State {
	DEAD,
	ALIVE,
}
