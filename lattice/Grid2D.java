//*******************************************************
// Grid2D.java
// created by Sawada Tatsuki on 2018/06/04.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* PAppletのラッパークラス */

import java.util.*;
import java.text.*;
import java.io.*;
import processing.core.*;

public abstract class Grid2D extends PApplet {
	protected static Simulator simulator; //シミュレータのインスタンス
	
	private int cellWidth = 1; //セルの横幅
	private int cellHeight = 1; //セルの縦幅
	private int hexRadius = 8; //hexセルの半径

	//正方セルのサイズを設定
	public void setCellSize(int cellWidth, int cellHeight) {
		this.cellWidth = cellWidth;
		this.cellHeight = cellHeight;
	}

	//六方セルのサイズを設定
	public void setHexSize(int hexRadius) {
		this.hexRadius = hexRadius;
	}
	
	//正方格子のウィンドウサイズを設定
	public void squareLattice(int row, int col) {
		size(row * cellWidth, col * cellHeight);
	}

	//六方格子のウィンドウサイズを設定
	public void hexLattice(int row, int col) {
		size((int)ceil(1.732050807568877f * row * hexRadius + 0.8660254037844386f * hexRadius), col * 3 * hexRadius / 2 + hexRadius / 2);
	}
	
	private int windowSize = 600; //ウィンドウサイズ
	private int terminate = -1; //終了ステップ数．-1 のとき無限ループ
	public int t; //経過ステップ数
	
    //コンストラクタ
    public Grid2D() {
		windowSize = 600; //ウィンドウサイズ
		simulator = new Simulator(); //シミュレータを生成
    }

	public abstract void controller(); //描画処理: オーバーライド前提
	
	public void draw(){
		simulator.run(); //系を動かす
		
		controller(); //描画処理: オーバーライド前提
		
		if(doesSave) {
			snap(); //スクリーンショットを保存
		}
		
		//終了条件
		if(t == terminate) {
			if(doesSave) {
				saveMovie(); //動画を保存
			}
			noLoop(); //描画停止
		}
	}

	/********** start: グリッドの描画 **********/
	//正方格子セルの描画
	public void cell(int col, int row) {
		int verX = row * cellWidth; //左上の座標x
		int verY = col * cellHeight; //左上の座標y
		int width = cellWidth; //幅
		int height = cellHeight; //高さ
		rect(verX, verY, width, height);
	}

	//六方格子セルの描画
	public void hexCell(int col, int row) {
		float shiftX = 0.8660254037844386f * hexRadius; //左上
		float shiftY = hexRadius; //左上
		float verX = 1.732050807568877f * row * hexRadius + shiftX; //中心の座標x
		float verY = 1.5f * col * hexRadius + shiftY; //左上の座標y

		if(col % 2 == 1) {
			verX += shiftX;
		}

		int width = cellWidth; //幅
		int height = cellHeight; //高さ

		pushMatrix();
		translate(verX, verY);
		beginShape();
		for (int i = 0; i < 6; i++) {
			vertex(hexRadius * sin(radians(60 * i)), hexRadius * cos(radians(60 * i)));
		}
		endShape(CLOSE);
		popMatrix();
	}
	/********** start: cellの描画 **********/

	public void fill(int c) {
		stroke(c);
		super.fill(c);
	}

	public void fill(int r, int g, int b) {
		stroke(r, g, b);
		super.fill(r, g, b);
	}
	
	/********** start: 終了条件の設定 **********/
	protected void setTerminate(int terminate) {
		this.terminate = terminate;
	}
	/********** end: 終了条件の設定 **********/
	
	/********** start: 日付の設定 **********/
	private String getDate() {
		Date date = new Date();  //Dateオブジェクトを生成
		SimpleDateFormat sdf = new SimpleDateFormat("YYMMdd'_'HHmmss"); //SimpleDateFormatオブジェクトを生成
		return sdf.format(date);
	}

	protected String date = getDate(); //シミュレータ実行時点の日時
	/********** end: 日付の設定 **********/
	
	/********** start: カウントラベルの表示処理 **********/	
	protected void showCountLabel(int t) {
		textSize(24); //文字サイズを指定
		String countLabel = "t = " + Integer.toString(t);
		text(countLabel, (float)0.04* windowSize, (float)0.064 * windowSize);
	}
	/********** end: カウントラベルの表示処理 **********/
	
	/********** start: 画像・動画の保存処理 **********/
	private boolean doesSave = false; //動画を保存するか決めるフラグ
	protected String dirPass = "frames/" + date + "/"; //スナップ保存先ディレクトリ
	
	//動画を保存するか否か
	protected void doesSave(boolean doesSave) {
		this.doesSave = doesSave;
	}

	//スクリーンショットを保存
	protected void snap() {
		saveFrame("frames/" + date + "/" + "######.png");//スクリーンショットを保存
	}

	//動画を自動生成
	protected void saveMovie() {
		try {
			Runtime rt = Runtime.getRuntime();
			String command = "ffmpeg -r 30 -start_number 0 -i " + dirPass + "%6d.png " + "-vframes " + 2 * (terminate + 1) + " -crf 1  -vcodec libx264 -pix_fmt yuv420p -r 60 " + dirPass + "/../" + date + ".mp4";
			rt.exec(command); //コマンド実行
			System.exit(0); //プログラム終了
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	/********** end: 画像・動画の保存処理 **********/
}
