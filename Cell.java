//*******************************************************
// Cell.java
// created by Sawada Tatsuki on 2018/06/05.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* セルのオブジェクト */

import java.util.*;

public class Cell extends AbstractCell {
	public static int col = 64; //行の数 (縦のセル数)
	public static int row = 64; //列の数 (横のセル数)

	public State state; //DEAD or ALIVE
	private State nextState; //DEAD or ALIVE
	
    //コンストラクタ
    public Cell(Simulator simulator, int idI, int idJ) {
		super(simulator, idI, idJ); //コンストラクタを継承
		size(col, row); //格子全体の大きさを設定
		init(); //初期化
    }
	
	//初期化
	public void init() {
		state = rnd.nextBoolean() ? State.DEAD : State.ALIVE; //ランダムな状態

		//壁は DEAD
		if(idI == 0 || idI == col - 1 || idJ == 0 || idJ == row - 1) {
			state = State.DEAD;
		}
	}

	private ArrayList<Cell> neighbors; //近傍の集合
	//観測フェーズ
	public void observe() {
		neighbors = get8Neighbors(simulator.cell); //近傍を観測
	}

	//状態更新フェーズ
	public void interact() {
		Iterator<Cell> iter = neighbors.iterator(); //リストのイテレータ
		
		int numAlive = 0; //生存セル数
		while(iter.hasNext()) {
			Cell neighbor = iter.next(); //近傍セル
			if(neighbor.state == State.ALIVE) {
				numAlive++; //生存セル数をカウント
			}
		}
		
		if(state == State.ALIVE) { //自分がALIVEのとき
			nextState = (numAlive == 2 || numAlive == 3) ? State.ALIVE : State.DEAD;
		} else if(state == State.DEAD) { //自分がDEADのとき
			nextState = (numAlive == 3) ? State.ALIVE : State.DEAD;
		}
	}
	
	//状態更新フェーズ
	public void update() {		
		state = nextState;
	}
}
