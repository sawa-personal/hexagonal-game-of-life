//*******************************************************
// Simulator.java
// created by Sawada Tatsuki on 2018/06/04.
// Copyright © 2018 Sawada Tatsuki. All rights reserved.
//*******************************************************
/* シミュレータのクラス */

public class Simulator extends AbstractSimulator {
	public int col = Cell.col; //行の数 (縦のセル数)
	public int row = Cell.row; //列の数 (横のセル数)

    public Cell[][] cell = new Cell[col + 2][row + 2]; //セル
	
    public Simulator() {
		//セルを生成
		for(int i = 0; i < col + 2; i++) {
			for(int j = 0; j < row + 2; j++) {
				cell[i][j] = new Cell(this, i, j);
			}
		}
    }

	//処理の内容
    public void process() {
		//観測フェーズ
		for(int i = 1; i < col + 1; i++) {
			for(int j = 1; j < row + 1; j++) {
				cell[i][j].observe();
			}
		}
		
		//相互作用フェーズ
		for(int i = 1; i < col + 1; i++) {
			for(int j = 1; j < row + 1; j++) {
				cell[i][j].interact();
			}
		}

		//更新フェーズ
		for(int i = 1; i < col + 1; i++) {
			for(int j = 1; j < row + 1; j++) {
				cell[i][j].update();
			}
		}
	}
}
